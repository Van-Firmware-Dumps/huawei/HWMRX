#!/bin/sh
#######################################################################
##- Copyright (c) Huawei Technologies Co., Ltd. 2019. All rights reserved.
##- @Description: start container, freerdp, save weston.log
##- @Author: 
##- @Create: 2022-06-06
#######################################################################*/
# set -e

while [ ! -e /data/log/hsl_share ]; do
    sleep 1
done

if [ -f /data/log/hsl_share/weston.log ]; then
    chmod 664 /data/log/hsl_share/weston.log
fi

chmod 1777 /data/log/hsl_share
mknod -m 0606 /data/log/hsl_share/out p
mknod -m 0606 /data/log/hsl_share/error p

echo '/data/log/hsl_share/%e.core.%p' > /proc/sys/kernel/core_pattern

SOCKETFILE=/dev/hsl/run/isulad.sock
while [ ! -e "$SOCKETFILE" ]; do
    sleep 1
done
chmod 1305 /dev/hsl
chmod 1305 /dev/hsl/run
mknod -m 0606 /dev/hsl/run/out p
mknod -m 0606 /dev/hsl/run/err p
mkdir -p /dev/hsl/rdp
chmod 1757 /dev/hsl/rdp
chmod 202 /dev/hsl/run/isulad.sock
